#!/usr/bin/env python3

##################################################################
###                        txt2json.py                         ###
###          Script para converter uma lista em JSON           ###
###                                                            ###
###            Pablo Alexander da Rocha Gonçalves              ###
###                parg.programador@gmail.com                  ###
###        https://github.com/parg-programador/Cargos          ###
##################################################################

from sys import argv
import simplejson as json

# Verifica se um String é válida
def isBlank (myString):
	if myString and myString.strip():
		#myString is not None AND myString is not empty or blank
		return False
	#myString is None OR myString is empty or blank
	return True

# cria uma lista
lista = []

# abre um arquivo apontado pelo parametro 1
f = open(argv[1])

# percorre o arquivo
for line in iter(f):
	if not isBlank(line):
		# adiciona na lista
		lista.append(line.strip())

# fecha o arquivo
f.close()

#transforma em json
print(json.dumps(lista))